module.exports = {
  presets: ['@babel/preset-env'],
  plugins: [
    ["@babel/plugin-transform-runtime", {
      corejs: 3
    }]
  ]
}
// 预设和插件的区别？ 预设就是一些插件的集合；预先设置好的插件
