import { add } from './a' // webpack默认可以省略的后缀是 .js  .json
import './css/index.css';
import './css/1.less';
import test1Img from './images/test1.png'

console.log(66)
console.log(add(1, 2))

window.onload = function () {
  document.getElementById('app2').innerHTML = '88888'
}

// qqq与BASEURL是在webpack中配置的全局变量
try {
  console.log(qqq, BASEURL);
} catch (error) {
  console.log(error.message)
}


// 看看图片的处理
let img = new Image();
img.src = test1Img
img.style.width = '70%'
document.body.appendChild(img)

// -es6语法-
class Man {
  static QQ = 123
  constructor(name, city) {
    this.name = name
    this.city = city
  }
  getName = () => {
    return this.name
  }
  getName1 = function () {
    return this.name
  }
  getName2 () {
    return this.name
  }
}
const f = new Man('李四', '上海')
console.log(f.getName())
console.log(f.getName1())
console.log(f.getName2())

console.log(f, Man.QQ);

async function getData () {
  let data = Promise.resolve(88)
  console.log(data);
}
getData()
// -es6语法-

console.log('test1212'.includes('test')); // 预设插件不支持它 需使用runtime-corejs3


//--查看不同环境下的baseurl--
console.log('BASEURL__:', BASEURL);


//
console.log(sdfs);
