// 这个文件是给webpack的配置文件 webpack是基于node编写的 所以webpack的配置是遵循commonJS规范的
let path = require('path') // path是一个node的内置模块 这个模块是专门用来处理路径的
let html = require('html-webpack-plugin')
let { CleanWebpackPlugin } = require('clean-webpack-plugin')
let webpack = require('webpack')
const cssExact = require('mini-css-extract-plugin') // 为了将css放到单独的css文件夹中
module.exports = {
  mode: 'development', // production
  entry: './src/main.js',// 入口文件默认是src下的index.js
  devtool: 'eval-cheap-module-source-map', // 推荐配置这个
  output: {
    // path: path.resolve(__dirname, 'qqq')
    path: path.resolve(__dirname, 'dist'),// 控制的是打包生成的文件的目录 这个path要求必须是绝对目录
    filename: 'index.[contenthash:8].js', // 控制的是生成的文件的名字 默认 main.js
    assetModuleFilename: 'imgs/[name][ext]' //图片打包后的存放以及名字处理

  },
  plugins: [
    // webpack的插件都是一个类
    new html({
      template: './public/index.html',// 指定这个html文件作为我们的html模板
      title: "webpack_test", // 会传给模板
      qqq: 66668887777,
      filename: "index.html",// 控制打包生成的html文件的名字
      minify: true,// 压缩生成的html 文件
      hash: true,// 控制的是link引入的css或者script引入js后边添加 ?[hash]
    }),
    new CleanWebpackPlugin(),
    // 定义一些编译阶段全局变量
    new webpack.DefinePlugin({
      //定义一下开发环境的api
      BASEURL: "'/api'",
    }),
    new cssExact({
      filename: 'index.[contenthash:8].css' // 设置名字，最终会link对应的css文件名的样式
    }),
  ],
  module: {
    // loader的加载顺序是从右向左 从下往上的；
    rules: [
      {
        test: /\.css$/, // 匹配那些以css结尾的文件
        // use: ['style-loader', 'css-loader']
        // 添加各浏览器厂商前缀的css属性--postcss-loader
        use: [cssExact.loader, 'css-loader', 'postcss-loader']
      },
      {
        test: /\.less$/, // 匹配那些以css结尾的文件
        // use: ['style-loader', 'css-loader', 'less-loader']
        use: [cssExact.loader, 'css-loader', 'less-loader', 'postcss-loader']

      },
      // 图片规则
      {
        test: /\.(jpg|png)$/,
        type: "asset/resource"
      },
      // js的规则
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/ // node_modules中的内容不需要使用babel-loader处理
      }
    ]
  }
}
